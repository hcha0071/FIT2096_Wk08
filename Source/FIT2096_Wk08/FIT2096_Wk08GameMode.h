// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FIT2096_Wk08GameMode.generated.h"

UCLASS(minimalapi)
class AFIT2096_Wk08GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFIT2096_Wk08GameMode();
};



