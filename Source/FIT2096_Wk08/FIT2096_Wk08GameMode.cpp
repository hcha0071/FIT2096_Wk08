// Copyright Epic Games, Inc. All Rights Reserved.

#include "FIT2096_Wk08GameMode.h"
#include "FIT2096_Wk08HUD.h"
#include "FIT2096_Wk08Character.h"
#include "UObject/ConstructorHelpers.h"

AFIT2096_Wk08GameMode::AFIT2096_Wk08GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFIT2096_Wk08HUD::StaticClass();
}
