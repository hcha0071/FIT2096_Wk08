// Fill out your copyright notice in the Description page of Project Settings.


#include "PhysicsBall.h"

// Sets default values
APhysicsBall::APhysicsBall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// ## Attach a mesh to this actor.
	m_ballComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ball Mesh"));
	m_ballComp->SetupAttachment(RootComponent);
	// ## Initialise Physics
	m_ballComp->SetSimulatePhysics(true);
	m_ballComp->SetMassScale(NAME_None, 10.0f); // Scale mass to 10.0f times what 
	//PhysX thinks it should be.There are no named skeletal bones to alter
}

// Called when the game starts or when spawned
void APhysicsBall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APhysicsBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

