// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "FIT2096_Wk08HUD.generated.h"

UCLASS()
class AFIT2096_Wk08HUD : public AHUD
{
	GENERATED_BODY()

public:
	AFIT2096_Wk08HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

