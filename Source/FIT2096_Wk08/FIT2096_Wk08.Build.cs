// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FIT2096_Wk08 : ModuleRules
{
	public FIT2096_Wk08(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
